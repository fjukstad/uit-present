uit-present
===========

University of Tromsø's graphic profile to [Present](http://godoc.org/code.google.com/p/go.talks/present).

HOWTO
==========
1. Install the [Present](http://godoc.org/code.google.com/p/go.talks/present),
	_go_ _get_ _code.google.com/p/go.talks/present_
2. Replace _$GOPATH/src/code.google.com/p/go.talks/present/static/styles.css_
with _static/styles.css_  
3. Replace _$GOPATH/src/code.google.com/p/go.talks/present/templates/slides.tmpl_ with _templates/slides.tmpl_
4. Place all _png_ images found in _static/_ into _$GOPATH/src/code.google.com/p/go.talks/present/static/_
5. Build it: _cd_ _$GOPATH/src/code.google.com/p/go.talks/present/_ and run _go_ _build_ or _go_ _install_ 
6. Run Present and open up a slide 


Example
=========
http://kox1.td.org.uit.no:3999/bjorn/inf-1100/1.slide
